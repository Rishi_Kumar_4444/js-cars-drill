const filterCarsByYear = (carYearsArray, limit) => {
  let filteredYears = [];

  for (let year of carYearsArray) {
    if (year < limit) {
      filteredYears.push(year);
    }
  }

  if (filteredYears !== null) {
    return filteredYears.length;
  } else {
    return "no cars found";
  }
};

module.exports = filterCarsByYear;
