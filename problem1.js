let findCarDetails = (inventory, carId) => {
  try {
    for (let car of inventory) {
      if (car.id === carId) {
        return `Car ${carId} is a ${car.car_year}, ${car.car_make} ${car.car_model}`;
      }
    }
  } catch (err) {
    return `${err} , car not found`;
  }
};

module.exports = findCarDetails;
