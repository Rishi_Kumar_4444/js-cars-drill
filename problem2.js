let lastCarDetails = (inventory) => {
  let lastCarId = 0;

  for (let car of inventory) {
    if (car.id > lastCarId) {
      lastCarId = car.id;
    }
  }

  if (lastCarId > 0) {
    return `Last car is a ${inventory[lastCarId - 1].car_make}  ${
      inventory[lastCarId - 1].car_model
    }`;
  } else {
    return `inventory is empty`;
  }
};

module.exports = lastCarDetails;
