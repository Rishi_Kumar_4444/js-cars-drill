let filterCarsByMake = (inventory,inputModel)=>{
   
    let filteredCars = []

   for(let car of inventory){

   if(car.car_make === inputModel){
    filteredCars.push(car)
   }

   if (filteredCars !== null) {
    return filteredCars;
  } else {
    return "cars not found";
  }

   }
}

module.exports = filterCarsByMake