const inventory = require("./data.js");
const filterCarsByMake = require("../problem6.js");

const audiCars = filterCarsByMake(inventory, "Audi");
const bmwCars = filterCarsByMake(inventory, "BMW");

console.log(JSON.stringify(audiCars), JSON.stringify(bmwCars));

