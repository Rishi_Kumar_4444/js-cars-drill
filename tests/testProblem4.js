const inventory = require("./data.js");
const getCarYears = require("../problem4.js");

const result = getCarYears(inventory);

console.log(result);

module.exports = result;
