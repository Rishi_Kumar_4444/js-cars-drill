const sortCarsByModel = (inventory) => {
  let sortedCars = inventory.sort((a, b) =>
    a.car_model.localeCompare(b.car_model)
  );

  if (sortedCars.length > 0) {
    return sortedCars;
  } else {
    return "data is not found";
  }
};

module.exports = sortCarsByModel;
